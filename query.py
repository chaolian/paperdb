#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 14 09:34:34 2018

@author: cl-iop
"""

from pymongo import MongoClient
client = MongoClient()
db = client.test_database
#db.posts.DeleteOne()
result = db.posts.find({'journal': 'Phys. Rev. Lett.'})
print([i for i in result])