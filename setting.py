#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun  1 21:51:09 2018

@author: clian
"""

import os

PDFFOLDER=os.getenv("HOME")+'/Desktop/Documents/'
BIBFILE=PDFFOLDER+'ref.bib'
