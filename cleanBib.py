#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 17 11:30:50 2018

@author: clian
"""

import bibtexparser
import arxivcheck
import argparse
class options(argparse.ArgumentParser):
    def __init__(self):
      super(options, self).__init__()
      
      self.add_argument('-i', '--input', type=str, nargs='?',
                        help='pdfFile')
      self.add_argument('-o', '--output', type=str, nargs='?',
                        help='doi')              
      self.add_argument('-a', '--abbrev_journal', type=bool, nargs='?',
                        help='abbrev_journal')    
      self.args = self.parse_args() 
      
if __name__ == '__main__':
  options = options()
  args = options.args
  
  ''' 
      three cases, 
       1. with only doi. Get bib from doi, if doi not found in ref.bib, add it.
       2. with only pdf, Try to extract doi from, 
          if doi is not found correctly in pdf, abort. If correct, add it to 
          ref.bib if doi not found
          add pdf if doi is found but without pdf
       3. with pdf and doi. Get bib from doi. add it to ref.bib if doi not found
          add pdf if doi is found but without pdf
  '''
  from arxivcheck.arxiv import generate_bib_from_arxiv, get_arxiv_info, check_arxiv_published
  from database import addBib
  #generate_bib_from_arxiv(get_arxiv_info('1901.00575')[1],value='1901.00575')

  bibtex_database = bibtexparser.load(open(args.input))
  dbdicts = bibtex_database.get_entry_dict()
  #arxixlist=[line.split('=')[1].replace(',','').replace('\n','').replace('{','').replace('}','') for line in open(args.input) if "arxivId" in line]
  arxivlist = [(dbdict,dbdicts[dbdict]['arxivid']) for dbdict in dbdicts 
               if 'arxivid' in list(dbdicts[dbdict].keys()) and 
               'doi' not in list(dbdicts[dbdict].keys())]
  for key, arxivid in arxivlist:
      found, published, bib = check_arxiv_published(arxivid, get_first=False)
      print(key)
      if published:
          value = list(bibtexparser.loads(bib).get_entry_dict().values())[0] 
          #
          dbdicts[key] = value
          #dbdicts.append(bibtexparser.loads(bib).get_entry_dict())
      else:
          #bib = get_bib_from_doi(doi,key=key,abbrev_journal=args.abbrev_journal)
          bib.replace('\textquotesingle',"'")
          biblist = bib.split('\n')
          biblist[0] = '@article{%s,'%key
          newtitle = biblist[3]
          newtitle = ''.join(['{%s}'%l if l.isupper() else l for l in newtitle])
          biblist[3] = newtitle
          bib = '\n'.join(biblist)
          print(bib)
          addBib(bib,filename=args.output)
#      if found:
#          if published:
#              bib = bib
#          else:
#              bib = generate_bib_from_arxiv(get_arxiv_info(arxivid)[1][0],value=arxivid)
#              biblist = bib.split('\n')
#              #print(key)
#              biblist[0] = '@article{%s,'%key
#              
#              print(bib)
#              addBib(bib,filename=args.output)
          
  #print()
  #print(dbdicts)
  doiNames = [(dbdict,dbdicts[dbdict]['doi']) for dbdict in dbdicts if 'doi' in list(dbdicts[dbdict].keys())]
  #arxivNames = [(dbdict,dbdicts[dbdict]['arxivId']) for dbdict in dbdicts if 'arxivId' in list(dbdicts[dbdict].keys())]
  #print(arxivNames)
  from addRefDB import get_bib_from_doi
  #from database import addBib
  #print(args.abbrev_journal)
  for key, doi in doiNames:
      bib = get_bib_from_doi(doi,key=key,abbrev_journal=args.abbrev_journal)
      print(bib)
      addBib(bib,filename=args.output)
      #print(key,doi)
  
      #print(bibtex_database[dbdict])
  
  
  # prefer user supplied doi
#  if args.doi is not None:
#    doi = args.doi
#  else:
#    if args.file is not None:
#      dois = get_doi_from_pdf(args.file)
#      doi = dois[0]
#    else:
#      raise AssertionError('No PDF file or DOI')
#      
  # warn user to check the bib
#  bib = get_bib_from_doi(doi)  
#  print(bib)
#  #from interact import query_yes_no
#  #confirm = query_yes_no('is this bib right?')
#  confirm = True
#  if confirm:
#    from database import checkExist, addBib, addPDF, addBibPDF
#    hasDOI, hasPDF = checkExist(bib)
#    if not hasDOI and args.file is None:
#      addBib(bib)
#    if not hasDOI and args.file is not None:
#      addBibPDF(bib,args.file)
#    if hasDOI and (not hasPDF) and (args.file is not None):
#      addPDF(bib,args.file)
#    
#    addBibItem, copyPDF
#    filename = addBibItem(bib,file=args.file)
#    if args.file is not None:
#      copyPDF(file)
