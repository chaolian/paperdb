#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 14 09:38:55 2018

@author: cl-iop
"""

from pymongo import MongoClient
client = MongoClient()
db = client.test_database
print(db.posts.find_one({'journal': 'Phys. Rev. Lett.'}))