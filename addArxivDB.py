#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 13 23:03:47 2018

@author: clian
"""



def get_bib_from_doi(filename,abbrev_journal=True):
  from arxivcheck.arxiv import generate_bib_from_arxiv, get_arxiv_info
  arxivID = filename[:-4]
  print(arxivID)
  found, items = get_arxiv_info(arxivID)
  print(items)
  return generate_bib_from_arxiv(items[0], arxivID)


import argparse
class options(argparse.ArgumentParser):
    def __init__(self):
      super(options, self).__init__()
      
      self.add_argument('-f', '--file', type=str, nargs='?',
                        help='pdfFile')
      self.add_argument('-d', '--doi', type=str, nargs='?',
                        help='doi')                     
      self.args = self.parse_args() 
      
if __name__ == '__main__':
  options = options()
  args = options.args
  if args.file is not None:
      bib = get_bib_from_doi(args.file)
      str_in = raw_input(str_s)