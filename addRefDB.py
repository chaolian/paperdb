#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 13 23:03:47 2018

@author: clian
"""



def get_bib_from_doi(doi,key=None,abbrev_journal=False):
    import doi2bib.crossref as dc
    #from bibtexparser import loads, dumps
    #from  doi2bib.crossref import get_bib_from_doi, get_json
    
    
    try:   
        found, item = dc.get_json(doi)
    except Exception as error:
        print(error)
        return ''
    if not found:
        print(doi)
        raise AssertionError('DOI not found')
    #print(item["message"])
    
    if len(item["message"]["short-container-title"]) != 0:
      #print(item["message"]["short-container-title"])
      abbreviated_journal = item["message"]["short-container-title"][0]
    else:
      abbreviated_journal = ''
    #else:
      #abbreviated_journal = item["message"]['container-title'][0]
    #
    pages = -1
    for pageName in 'article-number', 'page':
        if pageName in item["message"].keys():
            pages = item["message"][pageName]
            if '-' in pages:
                pages = pages.split('-')[0]    
    try:        
        found, bib = dc.get_bib(doi)
    except Exception as error:
        print(error)
        print(bib)
    
    
    bib.replace('\textquotesingle',"'")#.replace('}⃗','')
    if "@article" not in bib:
        bibnew = bib
    
    if key is None:
        key = doi[8:].replace('/','_')
    try: 
        biblines = bib.split('\n')
        biblines[0] = '\n@article{%s,'%key
        biblines.pop(1)
        newtitle = biblines[-3].replace('\textquotesingle',"'")
        newtitle = ''.join(['{%s}'%l if l.isupper() else l for l in newtitle])
        biblines[-3] = newtitle
        if abbrev_journal:
            if len(abbreviated_journal) != 0:   
                biblines[-2] ='\tjournal = {%s},'%abbreviated_journal
            else:
                print('no abbreviation of journal')
        else:
            biblines[-2] = biblines[-2]+','
        
        if 'pages' not in bib and pages != -1:
            biblines.insert(-1,'\tpages = {%s},'%pages)
        else:
            for index, line in enumerate(biblines):
                if 'pages' in line:
                    print(line)
                    biblines[index] = '\tpages = {%s},'%pages
                    
        biblines.insert(-1,'\tdoi={%s},'%doi)
        bibnew = '\n'.join(biblines)
    except Exception as error:
        print(error)
        print(bib)
    return bibnew



def get_doi_from_pdf(file):
  def _chopPeriod(x):
    if x[-1] in ('.','[',']','/','%'):
        return x[:-1]
    return x
  import re
  _doiRegexStr= re.compile(r'10[.][0-9]{4,}[^\s"/<>]*/[^\s"<>]+')
  import slate3k
  pdfData = slate3k.PDF(open(file,'rb')).text()
  dois = [_chopPeriod(x.lower()) for x in _doiRegexStr.findall(pdfData)]
  #open('Test.tex','w').write(pdfData)
  seen = set()
  return [x for x in dois if not (x in seen or seen.add(x))]

import argparse
class options(argparse.ArgumentParser):
    def __init__(self):
      super(options, self).__init__()
      
      self.add_argument('-f', '--file', type=str, nargs='?',
                        help='pdfFile')
      self.add_argument('-d', '--doi', type=str, nargs='?',
                        help='doi')                     
      self.args = self.parse_args() 
      
if __name__ == '__main__':
  options = options()
  args = options.args
  
  ''' 
      three cases, 
       1. with only doi. Get bib from doi, if doi not found in ref.bib, add it.
       2. with only pdf, Try to extract doi from, 
          if doi is not found correctly in pdf, abort. If correct, add it to 
          ref.bib if doi not found
          add pdf if doi is found but without pdf
       3. with pdf and doi. Get bib from doi. add it to ref.bib if doi not found
          add pdf if doi is found but without pdf
  '''
  # prefer user supplied doi
  if args.doi is not None:
    doi = args.doi
  else:
    if args.file is not None:
      dois = get_doi_from_pdf(args.file)
      doi = dois[0]
    else:
      raise AssertionError('No PDF file or DOI')
      
  # warn user to check the bib
  bib = get_bib_from_doi(doi)  
  print(bib)
  #from interact import query_yes_no
  #confirm = query_yes_no('is this bib right?')
  confirm = True
  if confirm:
    from database import checkExist, addBib, addPDF, addBibPDF
    hasDOI, hasPDF = checkExist(bib)
    if not hasDOI and args.file is None:
      addBib(bib)
    if not hasDOI and args.file is not None:
      addBibPDF(bib,args.file)
    if hasDOI and (not hasPDF) and (args.file is not None):
      addPDF(bib,args.file)
    
#    addBibItem, copyPDF
#    filename = addBibItem(bib,file=args.file)
#    if args.file is not None:
#      copyPDF(file)
    
    
