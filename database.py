#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 14 09:10:44 2018

@author: cl-iop
"""
from shutil import copyfile
import os

import setting
import bibtexparser

def checkExist(bib):
  
  if not os.path.exists(setting.BIBFILE):
    open(setting.BIBFILE,'w').write(' ')
    return False, False
    
  with open(setting.BIBFILE) as bibtex_file:
    bibtex_database = bibtexparser.load(bibtex_file)
    dbdict = bibtex_database.get_entry_dict()
    
    print(dbdict)
    #print(bib)
    item = list(bibtexparser.loads(bib).get_entry_dict().keys())[0]
    print(item)
    if item in dbdict.keys():
      hasDOI = True
      if dbdict[item]['file'] is None:
        hasPDF = False
      else:
        hasPDF = True
    else:
      hasDOI = False
      hasPDF = False
    
      
    #print(bibtex_database)  

  return hasDOI, hasPDF

def addBib(bib, filename=None):
  if filename is None: 
    open(setting.BIBFILE,'a').write(bib)
  else:
    open(filename,'a').write(bib)
  
def addPDF(bib,file):
  item = bibtexparser.loads(bib).get_entry_dict()[0]
  #item['file'] = newFilename
  infoDict = item
  journal = infoDict['journal'].replace(' ','_')
  title = infoDict['title'].replace(' ','_').replace('/','%')
  newFilename = r'%s|%s|%s.pdf'%(journal, infoDict['year'], title)
  copyfile(file, setting.PDFFOLDER+newFilename)
  with open(setting.BIBFILE,'rw') as bibtex_file:
    bibtex_database = bibtexparser.load(bibtex_file)
    dbdict = bibtex_database.get_entry_dict()
    dbdict[item.key()]['file'] = newFilename
    bibtexparser.dump(bibtex_database,bibtex_file)

def addBibPDF(bib,file):
  bibtex_database = bibtexparser.loads(bib)
  item = list(bibtex_database.get_entry_dict().values())[0]
  #item['file'] = newFilename
  #infoDict = item
  #print(item)
  journal = item['journal'].replace(' ','_')
  title = item['title'].replace(' ','_').replace('/','%')
  newFilename = r'%s|%s|%s.pdf'%(journal, item['year'], title)
  item['file'] = newFilename
  copyfile(file, setting.PDFFOLDER+newFilename)
  
  with open(setting.BIBFILE,'a') as bibtex_file:
    bibtexparser.dump(bibtex_database, bibtex_file)


def addBibItem(bib, file=None):
  #print('add this bib to database',bib)
  from bibtexparser import loads
  item = loads(bib).get_entry_list()[0]
  
  infoDict = item
  journal = infoDict['journal'].replace(' ','_')
  title = infoDict['title'].replace(' ','_').replace('/','%')
  newFilename = r'%s|%s|%s.pdf'%(journal, infoDict['year'], title)
  copyfile(file, setting.PDFFOLDER+newFilename)
  
  if file is not None:
    item['file'] = newFilename
  
  return newFilename
#  from pymongo import MongoClient
#  client = MongoClient()
#  db = client.test_database
#  db.posts.insert_one(item)
#  print(item)
  #print(db.posts.find_one(item))
  pass
