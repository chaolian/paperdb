# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

from PyPDF2 import PdfFileReader
import doi2bib.crossref
#import arxivcheck.arxiv
from shutil import copyfile
#import bibtexparser.bparser
import slate3k
import warnings
warnings.filterwarnings("ignore")

import re
_doiRegexStr= re.compile(r'10[.][0-9]{4,}[^\s"/<>]*/[^\s"<>]+')
print(_doiRegexStr)
import os
testSamples = [file for file in os.listdir('.') if file.split('.')[-1] == 'pdf']

def get_bib_from_doi(doi,abbrev_journal=True):
    found, bib = doi2bib.crossref.get_json(doi,abbrev_journal=True)
    key = doi[8:].replace('/','_')
    bibkeyEnd = bib.find('\n')
    bib = '@article{%s,'%key + bib[bibkeyEnd:] + '\n'
    
    doi2bib.crossref.get_json(doi)
    
# Open a PDF file.
def getPdfTxt(file):
  doc = slate3k.PDF(open(file,'rb'))
  #print(doc[0])
  return doc.text()
  
def _chopPeriod(x):
    if x[-1] in ('.','[',']','/','%'):
        return x[:-1]
    return x
  
def extractDois(pdfData):
   # pdfData = getPdfTxt(fname)#'\n'.join(getPdfTxt(fname))
    #dois = [_chopPeriod(x.lower()) for x in _doiRegexStr.findall(pdfData)]
    dois = [_chopPeriod(x.lower()) for x in _doiRegexStr.findall(pdfData)]
    open('Test.tex','w').write(pdfData)
    seen = set()
    return [x for x in dois if not (x in seen or seen.add(x))]
  
def getBib(doi, publisher):
  if publisher == 'arXiv':
    found, published, bib = arxivcheck.arxiv.check_arxiv_published(doi)
  else:
    found, bib = get_bib_from_doi(doi,abbrev_journal=True)
  if found:
    return bib #, abbreviated_journal = item["message"]["short-container-title"][0]
  else:
    print(doi, publisher)
    raise AssertionError('DOI not found')
    return ''

if __name__ == '__main__':
  
  import bibtexparser
  if os.path.exists('test.bib'):  
      with open('test.bib','r') as bibfile:
        inBib = bibtexparser.load(bibfile)
        bibdict = inBib.get_entry_dict()
        doiList = bibdict.keys()
  else:
      doiList = []
    
  #print(bibdict)
  listBibs = []
  for file in testSamples:
    #print()
    bibKey = file.split('DOI:')[-1][:-4]
    print(bibKey)
    included = bibKey in doiList
    print(included)
    if included: continue
  
    pdfData = getPdfTxt(file)
    dois = extractDois(pdfData) #getDOI(contents, publisher)
    print(dois)
    if 'science' in dois[0].lower() and len(dois) > 1:
      # stupid science layout, with first doi from previous paper, not current one.
      doi = dois[1]
    elif 'srep' in dois[0].lower():
      doi = dois[1]
    else:
      doi = dois[0]
    if doi in doiList:
      print('Found')
      infoDict = bibdict[doi[8:]]
      continue
    else:
      bib = getBib(doi, publisher='')
      
      key = doi[8:].replace('/','_')
      print(key)
      #print(bib)
      open('test.bib','a').write(bib)
      dbtemp = bibtexparser.loads(bib)
      infoDict = dbtemp.get_entry_dict()[key]
    
    journal = infoDict['journal'].replace(' ','_')
    title = infoDict['title'].replace(' ','_').replace('/','%')
    newFilename = r'%s-%s-%s_DOI:%s.pdf'%(journal, infoDict['year'], title,key)
    os.rename(file, newFilename)

  